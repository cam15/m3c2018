"""Lab 2 Task 2
This module contains functions for simulating Brownian motion
and analyzing the results
"""
import numpy as np
import matplotlib.pyplot as plt
import math

def brown1(Nt,M,dt=1):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    from numpy.random import randn

    #Initialize variable
    X = np.zeros((M,Nt+1))

    #1D Brownian motion: X_j+1 = X_j + sqrt(dt)*N(0,1)
    for i in range(M):
        for j in range(Nt):
            X[i,j+1] = X[i,j] + np.sqrt(dt)*randn(1)

    Xm = np.mean(X,axis=0)
    Xv = np.var(X,axis=0)
    return X,Xm,Xv


def analyze(M,display):
    """Complete this function to analyze simulation error
    """
    err = np.zeros(M)
    var = np.zeros(M)
    Mvalues=np.linspace(1,M+1,M)   'not right as M is wrong for log'
    lerr = np.zeros(M)
    lM = np.zeros(M)

    for j in range(M):             'not right here either'
        [a,b,Xvar] = brown1(100,j+1)
        var[j] = Xvar[100]
        err[j] = abs(100-Xvar[100])

    if display is True:
        for i in range(M):
            lerr[i] = math.log(err[i])
            lM[i] = math.log(Mvalues[i])

        plt.plot(lM,lerr)
        plt.show()

    return Mvalues,var,err
